import pygame

class Controller:
    
    def __init__(self, joystick, env_name):
        self.joystick = joystick
        self.env_name = env_name
        self.switch_button_last_state = False

        if self.joystick.get_name() == 'Trooper V2':
            self.type = 'ATARI'
        else:
            self.type = 'UNKNOWN'
    
    def check_switch(self, switch_button_idx):
        switch_button = self.joystick.get_button(switch_button_idx)
        # Pressed now but not pressed before
        if switch_button and not self.switch_button_last_state:
            switch = True
        else:
            switch = False

        # Record switch button state
        self.switch_button_last_state = switch_button
        return switch, switch_button

    def atari_to_action(self):

        action = None

        # 0 - Left
        # 1 - Noop
        # 2 - Right
        if self.env_name == 'MountainCar-v0':
            axis_0_val = self.joystick.get_axis(0)
            # left
            if axis_0_val < -0.5:
                action = 0
            # right
            elif axis_0_val > 0.5:
                action = 2
            # noop
            else:
                action = 1
            # Fire button
            switch, hold = self.check_switch(0)
        # 0 - NOOP
        # 1 - FIRE
        # 2 - RIGHT
        # 3 - LEFT
        # 4 - DOWN
        # 5 - DOWNRIGHT
        # 6 - DOWNLEFT
        # 7 - RIGHTFIRE
        # 8 - LEFTFIRE
        elif 'Enduro' in self.env_name:
            axis_0_val = self.joystick.get_axis(0)
            axis_1_val = self.joystick.get_axis(1)
            fire = self.joystick.get_button(0)

            # Fire overrides down
            if fire:
                # LEFTFIRE
                if axis_0_val < -0.5:
                    action = 8
                # RIGHTFIRE
                elif axis_0_val > 0.5:
                    action = 7
                # FIRE
                else:
                    action = 1
            # No Fire
            else:
                # DOWN
                if axis_1_val > 0.5:
                    # DOWNLEFT
                    if axis_0_val < -0.5:
                        action = 6
                    # DOWNRIGHT
                    elif axis_0_val > 0.5:
                        action = 5
                    # DOWN
                    else:
                        action = 4
                else:
                    # LEFT
                    if axis_0_val < -0.5:
                        action = 3
                    # RIGHT
                    elif axis_0_val > 0.5:
                        action = 2
                    # NOOP
                    else:
                        action = 0
            # Select Button
            switch, hold = self.check_switch(4)

        return action, switch, hold
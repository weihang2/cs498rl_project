import numpy as np
import sklearn
from sklearn import svm
import torch

class Classifier():

    def __init__(self, num_state_vars, num_actions):
        pass

    def train(self, states, actions):
        pass

    def eval(self, state):
        action = None
        confidence = 0
        return action, confidence

class Classifier_SVM(Classifier):

    def __init__(self, num_state_vars, num_actions):
        super().__init__(num_state_vars, num_actions)
        self.svm = sklearn.svm.SVC(probability=True)

    def train(self, states, actions):
        self.svm.fit(states, actions)

    def eval(self, state):
        state = state.reshape(1, -1)
        action_proba = self.svm.predict_proba(state)
        action = np.argmax(action_proba)
        confidence = action_proba[0, action]
        return action, confidence

class FCN(torch.nn.Module):

    def __init__(self, num_state_vars, num_actions):
        super().__init__()
        self.fc1 = torch.nn.Linear(in_features=num_state_vars, out_features=64)
        self.fc2 = torch.nn.Linear(in_features=64, out_features=num_actions)
        # self.fc3 = torch.nn.Linear(in_features=128, out_features=num_actions)
        self.relu = torch.nn.ReLU()
        self.dropout = torch.nn.Dropout(p=0.5)

    def forward(self, x):
        x = self.fc1(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.fc2(x)
        # x = self.relu(x)
        # x = self.dropout(x)
        # x = self.fc3(x)

        return x

class Classifier_FCN(Classifier):

    def __init__(self, num_state_vars, num_actions, GPU=True):
        super().__init__(num_state_vars, num_actions)
        # self.net = FCN(num_state_vars, num_actions)
        self.GPU = GPU
        self.num_state_vars = num_state_vars
        self.num_actions = num_actions
        # # Convert to GPU
        # if self.GPU:
        #     self.net = self.net.cuda()
        # Use Cross Entropy Loss and Adam optimizer
        self.criterion = torch.nn.CrossEntropyLoss()
        # self.optimizer = torch.optim.Adam(self.net.parameters())

    def train(self, states, actions, epochs = 100):
        self.net = FCN(self.num_state_vars, self.num_actions)
        # Convert to GPU
        if self.GPU:
            self.net = self.net.cuda()
        # Get size of state-action pairs
        size = states.shape[0]
        # Reset optimizer
        self.optimizer = torch.optim.Adam(self.net.parameters())
        for epoch in range(epochs):
            total_loss = 0
            states_tensor = torch.from_numpy(states.astype(np.float32))
            actions_tensor = torch.from_numpy(actions.astype(np.int64))
            # Convert to cuda
            if self.GPU:
                states_tensor = states_tensor.cuda()
                actions_tensor = actions_tensor.cuda()
            # Zero the gradient
            self.optimizer.zero_grad()
            # Predict + Optimize
            predictions = self.net(states_tensor)
            loss = self.criterion(predictions, actions_tensor)
            loss.backward()
            self.optimizer.step()

            # Compute mean loss
            avg_loss = loss.item()
            print('Epoch %d loss: %.3f' % (epoch, avg_loss))

            self.net.eval()
            with torch.no_grad():
                # Predict
                predictions = self.net(states_tensor)
                softmaxs = torch.nn.functional.softmax(predictions, dim=1)
                softmaxs_np = softmaxs.cpu().numpy()
                # Check correctness
                num_correct = np.count_nonzero(np.argmax(softmaxs_np, axis=1) == actions)
                accuracy = num_correct / size
                print('Accuracy: %.3f' % (accuracy))

            # for i in range(size):
            #     # Get state and action label
            #     state = torch.from_numpy(states[i].astype(np.float32).reshape(1, -1))
            #     action = torch.from_numpy(actions[i].astype(np.int64).reshape(1))
            #     # Convert to cuda
            #     if self.GPU:
            #         state = state.cuda()
            #         action = action.cuda()
            #     # Zero the gradient
            #     self.optimizer.zero_grad()
            #     # Predict + Optimize
            #     prediction = self.net(state)
            #     loss = self.criterion(prediction, action)
            #     loss.backward()
            #     self.optimizer.step()

            #     # Add loss
            #     total_loss += loss.item()
            # # Compute mean loss
            # avg_loss = total_loss / size
            # print('Epoch %d loss: %.3f' % (epoch, avg_loss))

            # # Eval
            # num_correct = 0
            # self.net.eval()
            # with torch.no_grad():
            #     for i in range(size):
            #         # Get state and action label
            #         state = torch.from_numpy(states[i].astype(np.float32).reshape(1, -1))
            #         action = torch.from_numpy(actions[i].astype(np.int64).reshape(1))
            #         # Convert to cuda
            #         if self.GPU:
            #             state = state.cuda()
            #             action = action.cuda()
            #         # Predict
            #         prediction = self.net(state)
            #         softmax = torch.nn.functional.softmax(prediction)
            #         softmax_np = softmax.cpu().numpy()
            #         # Check correctness
            #         if action == np.argmax(softmax_np):
            #             num_correct += 1
            # accuracy = num_correct / size
            # print('Accuracy: %.3f' % (accuracy))

    def eval(self, state):
        self.net.eval()
        with torch.no_grad():
            # Convert to tensor
            state = np.array(state).astype(np.float32).reshape(1, -1)
            state = torch.from_numpy(state)
            # Convert to cuda
            if self.GPU:
                state = state.cuda()
            # Predict
            prediction = self.net(state)
            softmax = torch.nn.functional.softmax(prediction, dim=1)
            softmax_np = softmax.cpu().numpy()
            action = np.argmax(softmax_np)
            confidence = softmax_np[0, action]
            return action, confidence

import gym
import pygame
import random
import sklearn
from sklearn import svm
import numpy as np
import joblib
import matplotlib.pyplot as plt

from controller import Controller
from dagger import Dagger, HGDagger, RandomAgent

joy_name = 'Trooper V2'
# Trooper V2: Atari Joystick

# MountainCar-v0
# Enduro-ramNoFrameskip-v0
env_name = 'Enduro-ramNoFrameskip-v0'

dagger_name = 'Dagger'

def eval(env, learner, num_episodes=1000, vis=False, fps=30, max_step=None):
    # Record total undiscounted return
    undiscounted_return_list = []
    for episode in range(num_episodes):
        done = False
        state = env.reset()
        undiscounted_return = 0
        t = 0
        while not done:
            # Visualization
            if vis:
                # Tick clock to be at 30 FPS (Otherwise too hard to play)
                clock.tick(fps)
                # Render environment
                env.render()
            # Act
            action = learner.act_no_expert(state)
            # Step
            next_state, reward, done, info = env.step(action)
            # Add reward
            undiscounted_return += reward
            # Continue to next state
            state = next_state
            # Increment time step
            t += 1
            # Check if reach the max step
            if t is not None and t >= max_step:
                break
        # Append to list
        undiscounted_return_list.append(undiscounted_return)
    # Close render window if vis
    if vis:
        env.close()
    return undiscounted_return_list

def get_mean_err(undiscounted_return_list):
    undiscounted_return_list = np.array(undiscounted_return_list)
    mean = np.mean(undiscounted_return_list)
    std = np.std(undiscounted_return_list)
    se = std / np.sqrt(undiscounted_return_list.shape[0])
    return mean, std, se

def plot_return(undiscounted_return_list, save=False):
    plt.hist(undiscounted_return_list)
    plt.xlabel('Returns')
    plt.ylabel('Frequency')
    if save:
        plt.savefig('hist.png')
    plt.show()

def plot_total_return(total_return_list, save=True, name='dagger'):
    npy_filename = name + ".npy"
    plot_filename = name + '.png'
    # Save numpy array
    np.save(npy_filename, total_return_list)
    # Aggregate between runs
    total_return_list = total_return_list.reshape(total_return_list.shape[0], -1)
    # Take mean of all the returns
    total_return_avg = np.mean(total_return_list, axis=1)
    total_return_std = np.std(total_return_list, axis=1)
    total_return_se = total_return_std / np.sqrt(total_return_list.shape[1])
    # Only use non-zero return
    valid_episode_indices = np.nonzero(total_return_avg)
    valid_return_avg = total_return_avg[valid_episode_indices]
    valid_return_se = total_return_se[valid_episode_indices]
    plt.xlabel('Number of Training Episodes')
    plt.ylabel('Average Return')
    plt.xticks(valid_episode_indices[0]+1)
    plt.errorbar(valid_episode_indices[0]+1, valid_return_avg, valid_return_se, capsize=5)
    if save:
        plt.savefig(plot_filename)
    plt.show()


if __name__ == '__main__':
    # Init pygame
    pygame.init()
    # Init clock
    clock = pygame.time.Clock()
    # Init joysticks
    pygame.joystick.init()
    # Get desired joystick
    joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
    for joystick in joysticks:
        if joystick.get_name() == 'Trooper V2':
            joystick.init()
            break

    # Init Human Controller
    controller = Controller(joystick, env_name)

    # Init environment
    env = gym.make(env_name)
    # Init frame rate depending on the game
    if env_name == 'MountainCar-v0':
        fps = 30
    elif 'Enduro' in env_name:
        fps = 30
    # Get information about enviornment
    num_state_vars = env.observation_space.shape[0]
    num_actions = env.action_space.n
    num_runs = 3
    num_episodes = 5
    eval_episode_step = 1
    num_eval_vis = 0
    num_eval_no_vis = 10
    max_step = 600

    if dagger_name == 'Random':
        num_episodes = 0
        num_eval_vis = 5

    total_return_list = np.zeros((num_episodes, num_runs, num_eval_no_vis))

    try:
        for run in range(num_runs):

            # Init learner
            if dagger_name == 'Dagger':
                learner = Dagger(num_state_vars, num_actions, p=0.8)
            elif dagger_name == 'HGDagger':
                learner = HGDagger(num_state_vars, num_actions)
            elif dagger_name == 'Cloning':
                learner = Dagger(num_state_vars, num_actions, p=1.0)
            elif dagger_name == 'Random':
                learner = RandomAgent(num_state_vars, num_actions)

            for episode in range(num_episodes):
                done = False
                state = env.reset()
                states = []
                expert_actions = []
                t = 0
                while not done:
                    # Get joystick information
                    pygame.event.pump()
                    # Tick clock to be at 30 FPS (Otherwise too hard to play)
                    clock.tick(fps)
                    # Render environment
                    env.render()
                    # Get expert action from joystick
                    expert_action, switch, hold = controller.atari_to_action()
                    # Act
                    if dagger_name == 'Dagger' or dagger_name == 'Cloning':
                        action = learner.act(state, expert_action)
                    elif dagger_name == 'HGDagger':
                        action = learner.act(state, expert_action, switch, hold)
                    # Step
                    next_state, reward, done, info = env.step(action)
                    state = next_state
                    # Increment time step
                    t += 1
                    # Check if reach the max step
                    if t is not None and t >= max_step:
                        break
                # Train learner
                learner.train()
                # Evaluate if episode meet eval step
                if episode % eval_episode_step == 0:
                    # Evaluation
                    undiscounted_return_list = eval(env, learner, num_episodes=num_eval_no_vis, max_step=max_step)
                    avg_return, std, se = get_mean_err(undiscounted_return_list)
                    print("Average Return: " + str(avg_return))
                    print("Standard Error: " + str(se))
                    print("Max Return: " + str(np.max(undiscounted_return_list)))
                    # Store to total return list
                    total_return_list[episode, run, :] = np.array(undiscounted_return_list)

            print("End run " + str(run))
            # # Close render window
            # env.close()
            # # Evaluation (Visualization)
            # print("Evaluation")
            # eval(env, learner, num_episodes=num_eval_vis, vis=True, fps=fps, max_step=max_step)
            # # Evaluation (Plot)
            # undiscounted_return_list = eval(env, learner, num_episodes=num_eval_no_vis, max_step=max_step)
            # avg_return, std, se = get_mean_err(undiscounted_return_list)
            # print("Average Return: " + str(avg_return))
            # print("Standard Error: " + str(se))
            # plot_return(undiscounted_return_list, save=False)

        env.close()
        plot_total_return(total_return_list, save=True, name=dagger_name)

    except KeyboardInterrupt:
        env.close()

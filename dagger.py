import numpy as np
import random
from classifier import Classifier_SVM, Classifier_FCN

class DaggerBase:

    def __init__(self, num_state_vars, num_actions):
        self.num_state_vars = num_state_vars
        self.num_actions = num_actions
        self.states = []
        self.actions = []
        self.i = 0
        self.classifier = None

    def train(self):
        self.classifier.train(np.array(self.states), np.array(self.actions))
        # Next episode
        self.i += 1

    def act(self, state, expert_action):
        return expert_action

    def act_no_expert(self, state):
        action, confidence = self.classifier.eval(state)
        return action

class RandomAgent(DaggerBase):

    def __init__(self, num_state_vars, num_actions):
        super().__init__(num_state_vars, num_actions)

    def act_no_expert(self, state):
        return random.randint(0, self.num_actions - 1)

class Dagger(DaggerBase):

    def __init__(self, num_state_vars, num_actions, p=0.8):
        super().__init__(num_state_vars, num_actions)
        self.p = p
        self.classifier = Classifier_FCN(num_state_vars, num_actions)

    def act(self, state, expert_action):
        # First episode force pass
        if self.i == 0:
            self.states.append(state)
            self.actions.append(expert_action)
            return expert_action
        # Beta that exp decays
        beta = self.p**self.i
        learner_action, confidence = self.classifier.eval(state)
        if random.random() < beta:
            action = expert_action
        else:
            action = learner_action
        self.states.append(state)
        self.actions.append(expert_action)
        return action


class HGDagger(DaggerBase):

    def __init__(self, num_state_vars, num_actions, switch_mode = 'hold'):
        super().__init__(num_state_vars, num_actions)
        self.doubt_state_log = []
        self.doubt_log = []
        # self.classifier = Classifier_SVM()
        self.classifier = Classifier_FCN(num_state_vars, num_actions)
        self.expert_has_control = True
        self.switch_mode = switch_mode

    def act(self, state, expert_action, switch, hold):
        # First episode force pass
        # Expert has control
        if self.i == 0:
            self.states.append(state)
            self.actions.append(expert_action)
            return expert_action
        # Get learner action and confidence
        learner_action, confidence = self.classifier.eval(state)
        if self.switch_mode == 'hold':
            self.expert_has_control = hold
            # Expert is taking control
            if switch:
                # Record doubt
                self.doubt_log.append(confidence)
                # Record doubt state
                self.doubt_state_log.append(state)
        elif self.switch_mode == 'switch' and switch:
            # Negate expert has control label
            self.expert_has_control = not self.expert_has_control
            # Expert is taking control
            if self.expert_has_control:
                # Record doubt
                self.doubt_log.append(confidence)
                # Record doubt state
                self.doubt_state_log.append(state)
        # Expert has control
        if self.expert_has_control:
            self.states.append(state)
            self.actions.append(expert_action)
            return expert_action
        else:
            return learner_action

import gym
import pygame
import random
import sklearn
from sklearn import svm
import numpy as np
import joblib

from controller import Controller

joy_name = 'Trooper V2'
# Trooper V2: Atari Joystick

env_name = 'Enduro-ramNoFrameskip-v4'


if __name__ == '__main__':
    # Init pygame
    pygame.init()
    # Init clock
    clock = pygame.time.Clock()
    # Init joysticks
    pygame.joystick.init()
    # Get desired joystick
    joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
    for joystick in joysticks:
        if joystick.get_name() == 'Trooper V2':
            joystick.init()
            break

    # Init Human Controller
    controller = Controller(joystick, env_name)

    # Init environment
    env = gym.make(env_name)
    # Get information about enviornment
    num_state_vars = env.observation_space.shape[0]
    num_actions = env.action_space.n
    num_episodes = 30
    num_eval_vis = 5
    num_eval_no_vis = 1000

    done = False
    state = env.reset()
    while not done:
        # Get joystick information
        pygame.event.pump()
        # Tick clock to be at 60 FPS
        clock.tick(60)
        # Render environment
        env.render()
        # Get expert action from joystick
        expert_action, switch, hold = controller.atari_to_action()
        # Step
        next_state, reward, done, info = env.step(expert_action)
        state = next_state

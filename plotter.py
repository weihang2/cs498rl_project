import numpy as np
import matplotlib.pyplot as plt

def plot_single(name, save=True):
    npy_filename = name + ".npy"
    plot_filename = name + '.png'
    # Load numpy array
    total_return_list = np.load(npy_filename)
    # Aggregate between runs
    total_return_list = total_return_list.reshape(total_return_list.shape[0], -1)
    # Take mean of all the returns
    total_return_avg = np.mean(total_return_list, axis=1)
    total_return_std = np.std(total_return_list, axis=1)
    total_return_se = total_return_std / np.sqrt(total_return_list.shape[1])
    # Only use non-zero return
    valid_episode_indices = np.nonzero(total_return_avg)
    valid_return_avg = total_return_avg[valid_episode_indices]
    valid_return_se = total_return_se[valid_episode_indices]
    plt.xlabel('Number of Training Episodes')
    plt.ylabel('Average Return')
    plt.xticks(valid_episode_indices[0]+1)
    plt.errorbar(valid_episode_indices[0]+1, valid_return_avg, valid_return_se, capsize=5)
    if save:
        plt.savefig(plot_filename)
    plt.show()

def plot_multi(names, names_on_plot, save=True, num_runs=3):
    plt.xlabel('Number of Training Episodes')
    plt.ylabel('Average Return')
    for name in names:
        npy_filename = name + ".npy"
        # Load numpy array
        total_return_list = np.load(npy_filename)
        # Aggregate between runs
        total_return_list = total_return_list.reshape(total_return_list.shape[0], -1)
        # Take mean of all the returns
        total_return_avg = np.mean(total_return_list, axis=1)
        total_return_std = np.std(total_return_list, axis=1)
        total_return_se = total_return_std / np.sqrt(total_return_list.shape[1])
        # Only use non-zero return
        valid_episode_indices = np.nonzero(total_return_avg)
        valid_return_avg = total_return_avg[valid_episode_indices]
        valid_return_se = total_return_se[valid_episode_indices]
        print(name)
        print(valid_return_avg[-1])
        print(valid_return_se[-1])
        plt.xticks(valid_episode_indices[0]+1)
        plt.errorbar(valid_episode_indices[0]+1, valid_return_avg, valid_return_se, capsize=5)
    plt.legend(names_on_plot)
    plt.title("Average Return over " + str(num_runs) + " Runs")
    if save:
        plt.savefig('all.png')
    plt.show()

if __name__ == '__main__':
    # plot_single('dagger')
    plot_multi(['Cloning', 'Dagger', 'HGDagger'], ['Behavior Cloning', 'DAGGER', 'HG-DAgger'])

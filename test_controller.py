import gym
import pygame
import random
import sklearn
from sklearn import svm
import numpy as np
import joblib

from controller import Controller

joy_name = 'Trooper V2'
# Trooper V2: Atari Joystick
env_name = 'Enduro-ramNoFrameskip-v4'

if __name__ == '__main__':
    # Init pygame
    pygame.init()
    # Init clock
    clock = pygame.time.Clock()
    # Init joysticks
    pygame.joystick.init()
    # Get desired joystick
    joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
    for joystick in joysticks:
        if joystick.get_name() == 'Trooper V2':
            joystick.init()
            break

    # Init Human Controller
    controller = Controller(joystick, env_name)

    while True:
        # Get joystick information
        pygame.event.pump()
        # Tick clock to be at 5 FPS
        clock.tick(30)

        # Get expert action from joystick
        expert_action, switch, hold = controller.atari_to_action()

        if switch:
            print("Switching")